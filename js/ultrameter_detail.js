$(document).ready(function() {
  $(".easytouse").click(function() {
    clearButtons();
    hideMessages();
    hideOnScreen();
    $(".easytouse_message").show();
    unilluminate();
    $(this).attr("src", "images/Details/easy_to_use_blue.png");
  });
  $(".mealflagging").click(function() {
    clearButtons();
    hideMessages();
    hideOnScreen();
    $(".mealflagging_message").show();
    $(this).attr("src", "images/Details/meal_flagging_blue.png");
  });
  $(".trusted").click(function() {
    clearButtons();
    hideMessages();
    unilluminate();
    $(".trusted_message").show();
    showOnScreen();
    $(this).attr("src", "images/Details/trusted_blue.png");
  });
});

function clearButtons () {
  $(".easytouse").attr("src", "images/Details/easy_to_use_grey.png");
  $(".mealflagging").attr("src", "images/Details/meal_flagging_grey.png");
  $(".trusted").attr("src", "images/Details/trusted_grey.png");
}

function unilluminate() {
  $("body").attr("class", "verioiq_background");
}

function hideMessages() {
  $(".message").hide();
}

function showOnScreen() {
  $(".onscreenmessage").show();
}

function hideOnScreen() {
  $(".onscreenmessage").hide();
}
