$(document).ready(function() {
	$(".compact").click(function() {
		clearButtons();
		hideMessages();
		hideMini();
		countUp();
		$(".compact_message").show();
		TweenMax.to($(".compact_message"), 0.5, {autoAlpha:1});
		$(this).attr("src", "images/Details/compact_blue.png");
	});
	$(".trusted").click(function() {
		clearButtons();
		hideMessages();
		hideMini();
		hideRuler();
		TweenMax.to($(".trusted_message"), 0.5, {autoAlpha:1});
//		$(".trusted_message").show();
		$(this).attr("src", "images/Details/trusted_blue.png");
	});

	$("#c_black").click(showMini);
});

var iCM=3.5;
var aPosIn=[398,499,560,606];

function countUp() {
	TweenMax.to($('#ruler'), 0.3, {autoAlpha:1, ease:Power0.easeInOut});
	iCM=3.5;
	TweenMax.to(window, 1.4, {iCM:10.5, onUpdate: function() {
		$("#ruler_text").text(String(Math.round(iCM*10)/10+" cm").replace(".",","));
	}});

	TweenMax.fromTo($('#ruler_img'), 1.4, {css:{width:0, height:18, x:404/2}}, {css:{width:404, height:18,x:0}});
}

function hideRuler() {
	TweenMax.to($('#ruler'), 0.2, {autoAlpha:0});
}

function showMini() {
	clearButtons();
	hideMessages();
	hideRuler();

	TweenMax.to($("#c0"), 0.5, {left:aPosIn[0], ease:Power1.easeOut});
	TweenMax.to($("#c1"), 0.5, {left:aPosIn[1], ease:Power1.easeOut, delay:0.4});
	TweenMax.to($("#c2"), 0.5, {left:aPosIn[2], ease:Power1.easeOut, delay:0.6});
	TweenMax.to($("#c3"), 0.5, {left:aPosIn[3], ease:Power1.easeOut, delay:0.8});
}

function hideMini() {
	TweenMax.to($("#c0"), 0.4, {left:1024});
	TweenMax.to($("#c1"), 0.4, {left:1024, delay:0.1});
	TweenMax.to($("#c2"), 0.4, {left:1024, delay:0.2});
	TweenMax.to($("#c3"), 0.4, {left:1024, delay:0.3});
}


function clearButtons () {
	$(".compact").attr("src", "images/Details/compact_grey.png");
	$(".trusted").attr("src", "images/Details/trusted_grey.png");
}

function hideMessages() {
	TweenMax.to($(".message"), 0.5, {autoAlpha:0});
//	$(".message").hide();
}


