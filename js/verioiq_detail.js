$(document).ready(function() {
  $("*").hammer().on("swiperight", function() {
    window.location.href = "index.html";
  });

  $("*").hammer().on("swipeleft", function() {
    window.location.href = "verioiq_close.html";
  });
    
    $("#learnmorelink").click(function() {
	$("#accuracy_popup").show();
	return false;
    });
    
    $(".closepopup").click(function() {
	$("#accuracy_popup").hide();
	return false;
    });

    

  $(".easytouse").click(function() {
    clearButtons();
    clearChart();
    hideMessages();
    hideOnScreen();
      $(".leftdot, .rightdot").show();
    $(".disclaimer").hide();
    $(".easytouse_message").show();
      $(".topleft").css("color", "#fff");
      $(".topleft").css("border-bottom", "2px solid #fff");
      $(".illteststrip").show();
      $("body").attr("class", "verioiq_background_illuminated");

    $(this).attr("src", "images/Details/easy_to_use_blue.png");
  });

    $(".rightdot").click(function() {
	clearChart();
	hideMessages();
	hideOnScreen();
	unilluminate();
	$(".leftdot").attr("src", "images/Details/darkdot.png");
	$(".tinybloodsample").show();
	
    });
    
    $(".leftdot").click(function() {
	clearButtons();
    clearChart();
    hideMessages();
    hideOnScreen();
      $(".leftdot, .rightdot").show();
    $(".disclaimer").hide();
    $(".easytouse_message").show();
      $(".topleft").css("color", "#fff");
      $(".topleft").css("border-bottom", "2px solid #fff");
      $(".illteststrip").show();
      $("body").attr("class", "verioiq_background_illuminated");

    $(".easytouse").attr("src", "images/Details/easy_to_use_blue.png");
 
    });
    
    
    
    $(".findshighsandlows").click(function() {
	clearButtons();
	clearChart();
	hideMessages();
	hideOnScreen();
	unilluminate();
	
	$(".disclaimer").hide();
	$(".findshighsandlows_message").show();
	
	unilluminate();
	$(".onscreenmessage").show();
	$(this).attr("src", "images/Details/findshighsandlows_selected.png");
  });




    $(".onesteptagging").click(function() {
	clearButtons();
	clearChart();
	$(".disclaimer").hide();
	hideMessages();
	unilluminate();
	hideOnScreen();
	$(".onesteptagging_message").show();
	$(".onesteptaggingscreen").show();
	$(this).attr("src", "images/Details/onesteptagging_selected.png");

    });

  $(".accuracy").click(function() {
    clearButtons();
    clearChart();

    $(".disclaimer").hide();
    hideMessages();
    unilluminate();
    hideOnScreen();
      $("#learnmore").show();

    $(this).attr("src", "images/Details/accuracy_selected.png");
  });


  showChart();
});

function showChart() {
  var pieOptions = {
    segmentShowStroke : true,
    segmentStrokeColor : "#2aaff8",
    segmentStrokeWidth : 6,
    animation : true,
    animationSteps : 30,
    animationEasing : "easeInOutSine",
    animateRotate : true,
    animateScale : false,
    onAnimationComplete : null
  };

  var pieData = [
    {
      value: 94,
      color:"#5bb4e5"
    },
    {
      value : 6,
      color : "#d6d6d6"
    }
  ];

  var myPie = new Chart(document.getElementById("chart").getContext("2d")).Pie(pieData, pieOptions);

}


function clearButtons () {
  $(".easytouse").attr("src", "images/Details/easy_to_use_grey.png");
  $(".findshighsandlows").attr("src", "images/Details/findshighsandlows.png");
  $(".onesteptagging").attr("src", "images/Details/onesteptagging.png");
  $(".accuracy").attr("src", "images/Details/accuracy.png");
    $(".leftdot, .rightdot").css("display", "none");
}

function clearChart() {
  TweenMax.to($(".chart"), 0.2, {autoAlpha:0});
}

function unilluminate() {
    $("body").attr("class", "verioiq_background");
    $(".topleft").css("color", "#005daa");
    $(".topleft").css("border-bottom", "2px solid #005daa");
    
}

function hideMessages() {
  $(".message").hide();
}

function showOnScreen() {
  $(".onscreenmessage").show();
}

function hideOnScreen() {
  $(".onscreenmessage").hide();
}

