$(document).ready(function() {
  if (getParameterByName("section") == "ultra") {
    $("#Image-Maps-Com-image-maps-2014-02-14-172235").attr("src", "images/accuracy/top_ultra.png");
    $(".ultramain").show();
    $(".veriomain").hide();
  }

  $(".ultra-btn").click(function() {
    $("#Image-Maps-Com-image-maps-2014-02-14-172235").attr("src", "images/accuracy/top_ultra.png");
    $(".ultramain").show();
    $(".veriomain").hide();
    return false;
  });

  $(".verio-btn").click(function() {
    $("#Image-Maps-Com-image-maps-2014-02-14-172235").attr("src", "images/accuracy/top_verio.png");
    $(".ultramain").hide();
    $(".veriomain").show();
    return false;
  });

  $(".details1-btn").click(function() {
    $(".veriopopup").show();
    $(".veriopopup").attr("src", "images/accuracy/verio_popup_1.png");
    return false;
  });

  $(".details2-btn").click(function() {
    $(".veriopopup").show();
    $(".veriopopup").attr("src", "images/accuracy/verio_popup_2.png");
    return false;
  });
  
  $(".close-btn").click(function() {
    $(".veriopopup").hide();
  });
    
  $(".accuracy").click(function() {
    clearButtons();

    $(".disclaimer").hide();

    hideMessages();
    hideOnScreen();
      $("#learnmore").show();
      $(".isodisclaimer").show();
    $(this).attr("src", "images/Details/accuracy_verio_selected.png");
  });
    
    $(".chooseIQ").click(function() {
	$("#Image-Maps-Com-image-maps-2014-09-02-130225").attr("src", "images/accuracy/accuracy_iqchosen.png");
	$("#Image-Maps-Com-image-maps-2014-09-02-141911").attr("src", "images/accuracy/100percent.png");
	$("#Image-Maps-Com-image-maps-2014-09-02-141911").show();
	$("#Image-Maps-Com-image-maps-2014-09-02-130225").css("top", "370px");
    });

    $(".chooseVerio").click(function() {
	$("#Image-Maps-Com-image-maps-2014-09-02-130225").attr("src", "images/accuracy/accuracy_veriochosen.png");
	$("#Image-Maps-Com-image-maps-2014-09-02-141911").attr("src", "images/accuracy/ysiplasma.png");
	$("#Image-Maps-Com-image-maps-2014-09-02-141911").show();
	$("#Image-Maps-Com-image-maps-2014-09-02-130225").css("top", "370px");
    });
    
    $(".closeinfo").click(function() {
	$("#Image-Maps-Com-image-maps-2014-09-02-141911").hide();
	$("#Image-Maps-Com-image-maps-2014-09-02-130225").css("top", "430px");
    });
    

    $(".details").click(function() {
	$("#Image-Maps-Com-image-maps-2014-09-02-141911").attr("src", "images/accuracy/500scans.png");
	$("#Image-Maps-Com-image-maps-2014-09-02-141911").show();
	
    });
});
