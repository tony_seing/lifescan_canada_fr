$(document).ready(function() {
  $("#btn-accuracy").click(function() {
    window.location.href = "accuracy_detail.html";
  });

  $("#btn-home").click(function() {
    window.location.href = "index.html";
  });

  $('body').css('opacity', '0').fadeTo(1900, 1,'swing'); 

});

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
  results = regex.exec(location.search);
  return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
