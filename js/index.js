
var aTL=[];
var iDuration = 1.2;
var iSlot = iDuration/6;
var iTarget=3;
var aTarget=[];
var iElements=6;
window._$MyAnim=this;

var aLink=[];


aLink.push("ultramini_detail.html");
aLink.push("ultra_detail.html");
aLink.push("verioiq_detail.html");
aLink.push("verio_detail.html");
aLink.push("accuracy_detail.html");
aLink.push("bestdays_overview.html");

var aPosition=[];
aPosition.push({x:664,y:60,scale:1.48});
aPosition.push({x:574,y:244,scale:1.27});
aPosition.push({x:524,y:283,scale:1.18});
aPosition.push({x:570,y:222,scale:1.1});
aPosition.push({x:315,y:-23,scale:1.07});
aPosition.push({x:650,y:180,scale:1.63});



window.onload = function() {

  var aP=["l","p"];

  for(var iIndex=0;iIndex<iElements;iIndex++) {

    console.log("Init item: "+iIndex);
    aTarget.push(iIndex);

    var img = $("#i"+iIndex);

    img.bClicker=true;

    var tl;

    tl = new TimelineMax({paused:true, repeat:-1, onReverseComplete: function() {
      console.log("Reverse loop");
      this.reverse(this._duration);
    }});
    tl.iItemIndex=iIndex;
    tl.iTargetSlot=iIndex;

    for(var nLoop=0;nLoop<2;nLoop++) {
      tl.to(img, 0, {x:57,y:255, zIndex:10, alpha: 0, scale: 0.35});
      tl.to(img, 0.1, {x:57,y:255, zIndex:10, alpha: 1, scale: 0.45, ease:Linear.easeInOut});
      tl.addCallback(checkDone, tl.duration(),[0, aP[nLoop]],tl);
      tl.addLabel(aP[nLoop]+"p0",tl.duration()+0.001);


      tl.to(img, iSlot, {x:179,y:295, zIndex:20, scale: 0.50, ease:Linear.easeInOut});
      tl.addLabel(aP[nLoop]+"p1");
      tl.addCallback(checkDone, tl.duration(),[1, aP[nLoop]],tl);

      tl.to(img, iSlot, {x:310,y:335, zIndex:30, scale: 0.62, ease:Linear.easeInOut});
      tl.addLabel(aP[nLoop]+"p2");
      tl.addCallback(checkDone, tl.duration(),[2, aP[nLoop]],tl);

      tl.to(img, iSlot, {x:520,y:355, zIndex:55, scale: 1.1, ease:Linear.easeInOut});
      tl.addLabel(aP[nLoop]+"p3");
      tl.addCallback(checkDone, tl.duration(),[3, aP[nLoop]],tl);

      tl.to(img, iSlot, {x:750,y:325, zIndex:20, scale: 0.62, ease:Linear.easeInOut});
      tl.addLabel(aP[nLoop]+"p4");
      tl.addCallback(checkDone, tl.duration(),[4, aP[nLoop]],tl);

      tl.to(img, iSlot, {x:942,y:290, zIndex:15, scale: 0.52,  ease:Linear.easeInOut});
      
      tl.addLabel(aP[nLoop]+"p5");
      tl.addCallback(checkDone, tl.duration(),[5, aP[nLoop]],tl);
	tl.to(img, 0.2, {x:1100,y:200, zIndex:10, scale: 0.45, ease:Linear.easeInOut});

    }

    aTL.push(tl);
  }

  for(var iIndex=0;iIndex<iElements;iIndex++) {
    aTL[iIndex].seek("pp"+iIndex);
  }

  TweenMax.to($("#carousel"),0.6,{alpha:1, ease:Linear.easeInOut});
  TweenMax.to($("#headline"),0.6,{alpha:1, ease:Linear.easeInOut});


  $("body").swipe({
    tap:function(event, target) {
      var sID=$(target).attr("id");
      switch(sID) {
      case "btn-accuracy":
	window.location.href="accuracy_detail.html";
	break;
      case "i0":
      case "i1":
      case "i2":
      case "i3":
      case "i4":
      case "i5":
	var n=sID.substr(1);
	for(var iIndex=0;iIndex<iElements;iIndex++) {
	  aTL[iIndex].stop();
	  var imgc = $("#i"+iIndex);
	  if(iIndex==n) {
	    TweenMax.to(imgc, 1, {scale:aPosition[n].scale, x:aPosition[n].x, y:aPosition[n].y, zIndex:200, onComplete: function() {
	      window.location.href=aLink[n];
	    }});
	  }
	  else
	  {
	    if(imgc.offset().left<520)
	      TweenMax.to(imgc, 1, {x:-200});
	    else
	      TweenMax.to(imgc, 1, {x:1200});
	  }
	}
	break;
      }
    },
    swipeRight:function(event, direction, distance, duration,fingerCount) {
      goNext();
    },
    swipeLeft:function(event, direction, distance, duration,fingerCount) {
      goPrev();
    }
  });

  console.log("init done!");

}

function checkDone(iIndex, sKind) {
  if(iIndex==this.iTargetSlot) {
    this.stop();
    if(sKind=="l") {
      this.seek("pp"+iIndex);
    }
  }
}



function goNext() {
  console.log("NEXT!");
  aTarget.push(aTarget.shift());
  if(iTarget>=iElements) iTarget=0;
  for(var iIndex=0;iIndex<iElements;iIndex++) {
    aTL[iIndex].iTargetSlot=aTarget[iIndex];
    aTL[iIndex].play();
  }
}

function goPrev() {
  console.log("PREV!");
  aTarget.unshift(aTarget.pop());
  //	console.log(aTarget);
  if(iTarget<0) iTarget=iElements-1;
  for(var iIndex=0;iIndex<iElements;iIndex++) {
    //		console.log(aTarget[iIndex]);
    aTL[iIndex].iTargetSlot=aTarget[iIndex];
    aTL[iIndex].reverse();
  }
}
