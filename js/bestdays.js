$(document).ready(function() {
  

  $("*").hammer().on("swiperight", function() {
    window.location.href = "bestdays_overview.html";
  });

  $("*").hammer().on("swipeleft", function() {
    window.location.href = "index.html";
  });
 

  $(".monitoring_link").click(function() {
    $("#Image-Maps-Com-image-maps-2014-02-14-142151").attr("src", "images/bestdays/bestdays_monitoring_screen.png");
    $(".lowertext2, .lowertext3, .lowertext4").hide();
    $(".lowertext1").show();
    
    return false;
  });

  $(".eatingwell_link").click(function() {
    $("#Image-Maps-Com-image-maps-2014-02-14-142151").attr("src", "images/bestdays/bestdays_eat_smart_screen.png");
    $(".lowertext1, .lowertext3, .lowertext4").hide();
    $(".lowertext2").show();
    return false;
  });

  $(".livingwell_link").click(function() {
    $("#Image-Maps-Com-image-maps-2014-02-14-142151").attr("src", "images/bestdays/bestdays_living_healthy_screen.png");
    $(".lowertext1, .lowertext2, .lowertext4").hide();
    $(".lowertext3").show();
    return false;
  });

  $(".enjoyinglife_link").click(function() {
    $("#Image-Maps-Com-image-maps-2014-02-14-142151").attr("src", "images/bestdays/bestdays_enjoying_life_screen.png");
    $(".lowertext1, .lowertext1, .lowertext3").hide();
    $(".lowertext4").show();
    return false;
  });


});
